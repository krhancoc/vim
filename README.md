Pull git repo

git clone https://git.uwaterloo.ca/krhancoc/vim.git ~/.vim

ln -s ~/.vim/vimrc ~/.vimrc

Open vim

Type ":PlugInstall"


Command to create ctags:

Go to main directory for where you will be working (root directory of the project).
Use the command

ctags -R .

Commands:

ctrl + ] (goto symbol on mouse)
ctra + t (goback to previous location)

Think of it as a stack!